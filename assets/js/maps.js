// Checks to see what map files exist at /usb-butter/osm-map-files/ and displays them
// using the template hidden in the page

// APKs which would be acceptable to link to in the instructions
// for installing maps/OSMAnd.  The first one found will be used.
OSM_PACKAGES = ["net.osmand", "net.osmand.plus"];

const getMaps = async (folder_href) => {
    async function populateSpan(response) {
        if (!response.ok) {
            console.error("Failed to fetch " + folder_href);
            return;
        }
        const text = await response.text();
        const doc = new DOMParser().parseFromString(text, 'text/html');
        const { files, folders } = extractDirectoryListing(doc);
        renderMaps(files);
    }
    const response = fetch(folder_href).then(populateSpan);
}

function getOsmObfDisplayName(name) {
    const region = name.split('_')[0];
    const words = region.split('-');
    for (let i = 0; i < words.length; i++) {
        words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
    }
    return words.join(" ");
}

function renderMaps(files) {
    template = document.getElementById('filerow-template');
    fileList = template.parentElement;
    for (let file of files) {
        let clone = template.cloneNode(true);
        const links = clone.querySelectorAll('a');
        for (let link of links) {
            link.href = '../usb-butter/osm-map-files/' + file.href;
            link.download = file.name
        }
        const upperText = clone.querySelector('.upper-text');
        const lowerText = clone.querySelector('.lower-text');
        upperText.textContent = getOsmObfDisplayName(file.name);
        lowerText.textContent = file.name;
        fileList.appendChild(clone);
        clone.removeAttribute('id');
        clone.classList.remove("template");
    }
}

// If OSM is in the F-Droid repo, show the download button
// and point to proper apk with proper size.
const getOsm = async () => {
    repoRoot = '../usb-butter/appstore/fdroid/repo/';
    json_url = repoRoot + 'index-v1.json';
    const response = fetch(json_url).then(async (response) => {
        const ix = await response.json();
        for (let package of OSM_PACKAGES) {
            if (ix.packages && ix.packages[package] && ix.packages[package].length > 0) {
                const button = document.getElementById('osm-dl-button');
                const links = button.querySelectorAll('a');
                const apkName = ix.packages[package][0]['apkName'];
                const size = ix.packages[package][0]['size'];
                const sizeInMB = Math.floor(size / 1024 / 1024);
                for (let link of links) {
                    link.href = repoRoot + apkName;
                }
                const cta = button.querySelector('.button-sub-text');
                cta.textContent = `${sizeInMB} MB`;
                button.classList.remove('hidden');
                break;
            }
        }
    });
}

getOsm()
getMaps('../usb-butter/osm-map-files/');