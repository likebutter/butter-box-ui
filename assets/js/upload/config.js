// Change this file as you need it

const config = {
	port: 5001,
	gateway: 8080
}

const node = {
	default: 'local',
	remote: {
		address: 'ipfs.sea.tube',
		...config,
		protocol: 'https'
	},

	// default local node
	// $butter_name is replaced with the butterbox's address during provisioning
	// see https://gitlab.com/likebutter/butterbox-rpi
	local: {
		address: window.location.hostname,
		...config,
		protocol: 'http'
	}
}
