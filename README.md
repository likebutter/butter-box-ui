# Butter Box UI

Run locally:

`bundle exec jekyll serve`

## Dummy USB UI

In addition to running the main UI locally, you can also simulate a USB drive having been inserted.  To do so, make a suitable directory tree available at `usb-butter` by running:

`cp -r simulated-usb-butter usb-butter`

Of course, this is just the UI layer, but it lets you work with the JS and CSS without needing a real box running.

You can take this a step further and simulate an fdroid webdash content pack.  Use `fdroid-webdash-butter` with a sample app store or the wind-repo.  See `App Store.md`